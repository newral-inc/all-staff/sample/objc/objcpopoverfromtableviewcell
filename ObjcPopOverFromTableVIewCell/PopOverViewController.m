//
//  PopOverViewController.m
//  ObjcPopOverFromTableVIewCell
//
//  Created by mfuta1971 on 2019/04/21.
//  Copyright © 2019 paveway. All rights reserved.
//

#import "PopOverViewController.h"

@interface PopOverViewController (){
    UIView *secondView;
    UILabel *popOverLabel;
}

@end

@implementation PopOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self initLabel];
    
    // Do any additional setup after loading the view.
}

-(void)initView{
    secondView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 400.0f, 44.0f)];
    secondView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:secondView];
}

-(void)initLabel{
    popOverLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 400.0f, 44.0f)];
    popOverLabel.text = @"UIPopoverPresentationController!!";
    [secondView addSubview:popOverLabel];
}

@end
