//
//  ViewController.m
//  ObjcPopOverFromTableVIewCell
//
//  Created by mfuta1971 on 2019/04/21.
//  Copyright © 2019 paveway. All rights reserved.
//

#import "ViewController.h"
#import "PopOverViewController.h"

// 参考
//【Objective-C】TableViewで押下したセルからポップオーバーを出す実装（UIPopoverPresentationController使用）【Xcode10.1】
// https://note.mu/iga34engineer/n/n4ffece5f0622

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate>

@end

@implementation ViewController{
    //インスタンス変数の宣言　//これにより、このインスタンスのインスタンスメソッドからアクセスが可能になる。
    //各インスタンスメソッドでアクセスする時は tableViewでアクセス可能。
    UITableView *tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //tableViewの初期化
    //initWithFrame（初期化しつつFrame指定している）
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStylePlain];
    
    /*
     tableViewにはUITableViewDelegate,UITableViewDataSource 2つのデリゲートが存在しており、
     「delegateを処理するのはこのインスタンス(self)ですよ」と宣言する必要がある。
     */
    tableView.delegate = self;
    tableView.dataSource = self;
    
    
    //このインスタンスのViewにtabelViewを描画する。
    [self.view addSubview:tableView];
}

#pragma #UITableView Datasource(実装必須)

//row = 行数を指定するデリゲートメソッド
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //必ずNSInteger型を返してあげている。
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //標準で用意されているTableViewを利用する場合。
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"このセルは%ld番目のセルになります！", (long)indexPath.row];
    return cell;
}

#pragma #UITableView Delegate(実装必須)
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //押下したセルからポップオーバーを出す為にindexPath.row番目のセルを、
    //UItableViewCell型で宣言してセルのインスタンスを取得している。
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
    PopOverViewController *popOverVC = [[PopOverViewController alloc]init];
    //sourceView = ポップオーバーを出すオブジェクトという認識
    [self presentPopOverWithViewController:popOverVC sourceView:selectedCell];
}

#pragma #UIPopoverPresentationControllerDelegate

- (void)presentPopOverWithViewController:(UIViewController *)viewController sourceView:(UIView *)sourceView
{
    viewController.modalPresentationStyle = UIModalPresentationPopover;
    viewController.preferredContentSize = CGSizeMake(400.0, 44.0);
    
    UIPopoverPresentationController *presentationController = viewController.popoverPresentationController;
    presentationController.delegate = self;
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    presentationController.sourceView = sourceView;
    presentationController.sourceRect = sourceView.bounds;
    
    //吹き出しの根本が色変わるのでおすすめ。
    presentationController.backgroundColor = [UIColor whiteColor];
    
    
    [self presentViewController:viewController animated:YES completion:NULL];
}

//iPhoneでの描画に大きく関係するのでしっかり追加しておく
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}

@end
